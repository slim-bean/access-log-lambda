### Setup:

Ansible dependencies

```
sudo apt install ansible python-pip
pip install boto boto3
```

*NOTE* All ansible commands have to be run from within the ansible subdirectory!

```
cd ansible
```

Create a vault password file to save some keystrokes, the included `ansible.cfg` file includes a parameter to reference this file.

```
echo "super_secret_pass" > .vault_pass
```

Remove my Ansilbe Vault creds file and create your own:

```
rm group_vars/all/creds.yml
ansible-vault create group_vars/all/creds.yml
```

You'll need these two entries:

```yaml
ec2_access_key: ""
ec2_secret_key: ""
```

You'll also want to setup your own ssh key for retrieving files from git

```
rm ssh_key/id_rsa
ssh-keygen -t rsa -b 4096 -f ssh_key/id_rsa
```

You'll probably want to use a blank password.

Then encrypt this file with ansible vault:

```
ansible-vault encrypt ssh_key/id_rsa
```

Checkout `group_vars/all/vars.yml` and update anything if required, you may want to check the subnet range I picked and make sure it doesn't overlap any existing subnets you have in use

Make sure the rust version matches your dev environment

Make sure the Amazon AMI image matches the current amazon environment: http://docs.aws.amazon.com/lambda/latest/dg/current-supported-versions.html

Now you should be able to provision the Rust build server:

```
ansible-playbook start_build_server.yml
```


When you are done, this will also cleanup and remove the VPC:

```
ansible-playbook terminate_build_server.yml
```

### Build Requirements:

rust-openssl requires a few additional requirements:

```
sudo apt install pkg-config libssl-dev
```

### Running the python test wrapper

You can execute test.py to invoke the handler as AWS would, tweak test_event.json to your liking

You also need several environment variables set:

```
RUST_BACKTRACE=1
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
SO_PATH=../target/debug/libaccessloglambda.so
```

Set your AWS key's accordingly.

If running from IDEA you can just add these to the run configuration

