extern crate rusoto_core;
extern crate rusoto_s3;
extern crate time;
extern crate flate2;


use std::ffi::CStr;
use std::os::raw::c_char;
use std::env;
use std::str::FromStr;
use std::io::{BufRead, BufReader, Read};
use std::error::Error;
use rusoto_s3::{S3, S3Client, GetObjectRequest};
use rusoto_core::{DefaultCredentialsProvider, Region};
use rusoto_core::default_tls_client;
use time::PreciseTime;
use flate2::read::MultiGzDecoder;

#[cfg(test)]
mod mock_s3;

struct S3ObjectId {
    bucket_name: String,
    object_name: String,
    region: String,
}

#[no_mangle]
pub extern "C" fn handle(bucket_name: *const c_char, object_name: *const c_char, region: *const c_char) -> i32 {
    println!("handle called");
    //Convert the incoming object_name and region into Rust strings (value is copied)
    let bucket_name: String = unsafe { CStr::from_ptr(bucket_name).to_string_lossy().into_owned() };
    let object_name: String = unsafe { CStr::from_ptr(object_name).to_string_lossy().into_owned() };
    let region: String = unsafe { CStr::from_ptr(region).to_string_lossy().into_owned() };

    //Handle the event
    handler(S3ObjectId { bucket_name: bucket_name, object_name: object_name, region: region })
}

fn handler(object_id: S3ObjectId) -> i32 {
    println!("Bucket Name: {}, Object Name: {}, Region: {}", object_id.bucket_name, object_id.object_name, object_id.region);

//    let test_mode = env::var("TEST").is_ok();

    let provider = DefaultCredentialsProvider::new().unwrap();
    let client = S3Client::new(default_tls_client().unwrap(), provider, Region::from_str(&object_id.region).unwrap());



    process(&client, object_id);

    0
}

pub struct ProcessResult{
    pub lines: u32,
    pub bytes: u64,
    pub time: i64
}

fn process(client: &S3, object_id: S3ObjectId) -> ProcessResult {
    let start = PreciseTime::now();
    //TODO: This is a poor mans test to see if the file is compressed, we should really use the check header functions in the gzip library, the problem is we can't easily do this without consuming the stream
    let gzipped = object_id.object_name.ends_with(".gz");
    let object = retrieve_object(client, object_id);
    let mut line_count: u32 = 1;
    let mut byte_count: u64 = 0;
    if object.is_some() {
        let gzip_read = object.unwrap();

        //Create a trait object so we can get a little polymorphism on how we read the file, only using a gzip decoder if the file is gzipped
        let buff_reader: Box<BufRead> = if gzipped {
            //Note the MultiGzDecoder will also create it's own internal buffer which is quite large and we don't have control over the size
            //keep this in mind when sizing the write array size so we don't run out of memory in our Lambda
            Box::new(BufReader:: new (MultiGzDecoder::new(gzip_read)))
        } else {
            Box::new(BufReader::new( gzip_read))
        };

        for line in buff_reader.lines() {
            //Line 2 is just a comment with column names, skip it
            if line_count == 2 {
                line_count +=1 ;
                continue
            }

            //I'm not sure there is a scenario where the value would not be OK?
            if line.is_ok() {
                let line_val = line.unwrap();

                //Count the bytes in the line, add 1 for the line return
                byte_count += line_val.len() as u64 + 1;

                //First line should be version string, check it and make sure it's 1.0
                if line_count == 1 {
                    if !line_val.starts_with("#Version:") {
                        panic!("First line of the file was not the version string!");
                    }
                    //Slice the string to check
                    if "1.0".ne(&line_val[10..]) {
                        panic!("File format is not the expected version 1.0, instead found {}", line_val)
                    }
                    //Version check passed
                    line_count += 1;
                    continue;
                }

                println!("{}", line_val);
                line_count += 1;
            }

        }

    }

    let duration_ms = start.to(PreciseTime::now()).num_milliseconds();
    println!("Retrieving and parsing took {}ms",  duration_ms);
    ProcessResult{
        //We increment the linecount while processing each line, so the actual count will be one less than our counter
        lines: line_count - 1,
        bytes: byte_count,
        time: duration_ms,
    }
}

fn retrieve_object(client: &S3, object_id: S3ObjectId) -> Option<Box<Read>> {
    let request = GetObjectRequest {
        key: object_id.object_name,
        part_number: None,
        range: None,
        request_payer: None,
        response_cache_control: None,
        response_content_disposition: None,
        response_content_encoding: None,
        response_content_language: None,
        response_content_type: None,
        response_expires: None,
        sse_customer_algorithm: None,
        sse_customer_key: None,
        sse_customer_key_md5: None,
        bucket: object_id.bucket_name,
        if_match: None,
        if_modified_since: None,
        if_none_match: None,
        if_unmodified_since: None,
        version_id: None,
    };
    match client.get_object(&request) {
        Ok(out) => {
            Some(Box::new(out.body.unwrap()))
        }
        Err(e) => {
            println!("Failed to Retrieve Object: {}", e.description());
            if e.cause().is_some() {
                println!("Underlying Cause: {}", e.cause().unwrap().description())
            }
            None
        }
    }
}





#[cfg(test)]
mod tests {
    use super::*;
    use mock_s3::{LocalFileS3Client, GeneratedFileS3Client};

    #[test]
    fn test_small_gzipped_file() {

        let client = LocalFileS3Client{
            file_name: String::from("test/test_small.gz"),
        };
        let object_id = S3ObjectId{
            bucket_name: String::new(),
            //The name here isn't overly important however we do check the extension to see if the file is compressed or not
            object_name: String::from(".gz"),
            region: String::new(),
        };

        let processed = process(&client, object_id);

        assert_eq!(26, processed.lines);
    }

    #[test]
    fn test_generated_file() {

        let client = GeneratedFileS3Client{
            lines: 25000,
        };
        let object_id = S3ObjectId{
            bucket_name: String::new(),
            object_name: String::new(),
            region: String::new(),
        };

        let processed = process(&client, object_id);

        assert_eq!(25000, processed.lines);
    }
}
