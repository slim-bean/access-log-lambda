

use rusoto_s3::*;
use std::fs::File;
use std::result::Result;
use std::io::Read;
use std::io;
use std::str;



pub struct LocalFileS3Client {
    pub file_name:String
}


#[allow(unused_variables, warnings)]
impl S3 for LocalFileS3Client{
    fn abort_multipart_upload(&self, input: &AbortMultipartUploadRequest) -> Result<AbortMultipartUploadOutput, AbortMultipartUploadError> {
        unimplemented!()
    }

    fn complete_multipart_upload(&self, input: &CompleteMultipartUploadRequest) -> Result<CompleteMultipartUploadOutput, CompleteMultipartUploadError> {
        unimplemented!()
    }

    fn copy_object(&self, input: &CopyObjectRequest) -> Result<CopyObjectOutput, CopyObjectError> {
        unimplemented!()
    }

    fn create_bucket(&self, input: &CreateBucketRequest) -> Result<CreateBucketOutput, CreateBucketError> {
        unimplemented!()
    }

    fn create_multipart_upload(&self, input: &CreateMultipartUploadRequest) -> Result<CreateMultipartUploadOutput, CreateMultipartUploadError> {
        unimplemented!()
    }

    fn delete_bucket(&self, input: &DeleteBucketRequest) -> Result<(), DeleteBucketError> {
        unimplemented!()
    }

    fn delete_bucket_analytics_configuration(&self, input: &DeleteBucketAnalyticsConfigurationRequest) -> Result<(), DeleteBucketAnalyticsConfigurationError> {
        unimplemented!()
    }

    fn delete_bucket_cors(&self, input: &DeleteBucketCorsRequest) -> Result<(), DeleteBucketCorsError> {
        unimplemented!()
    }

    fn delete_bucket_inventory_configuration(&self, input: &DeleteBucketInventoryConfigurationRequest) -> Result<(), DeleteBucketInventoryConfigurationError> {
        unimplemented!()
    }

    fn delete_bucket_lifecycle(&self, input: &DeleteBucketLifecycleRequest) -> Result<(), DeleteBucketLifecycleError> {
        unimplemented!()
    }

    fn delete_bucket_metrics_configuration(&self, input: &DeleteBucketMetricsConfigurationRequest) -> Result<(), DeleteBucketMetricsConfigurationError> {
        unimplemented!()
    }

    fn delete_bucket_policy(&self, input: &DeleteBucketPolicyRequest) -> Result<(), DeleteBucketPolicyError> {
        unimplemented!()
    }

    fn delete_bucket_replication(&self, input: &DeleteBucketReplicationRequest) -> Result<(), DeleteBucketReplicationError> {
        unimplemented!()
    }

    fn delete_bucket_tagging(&self, input: &DeleteBucketTaggingRequest) -> Result<(), DeleteBucketTaggingError> {
        unimplemented!()
    }

    fn delete_bucket_website(&self, input: &DeleteBucketWebsiteRequest) -> Result<(), DeleteBucketWebsiteError> {
        unimplemented!()
    }

    fn delete_object(&self, input: &DeleteObjectRequest) -> Result<DeleteObjectOutput, DeleteObjectError> {
        unimplemented!()
    }

    fn delete_object_tagging(&self, input: &DeleteObjectTaggingRequest) -> Result<DeleteObjectTaggingOutput, DeleteObjectTaggingError> {
        unimplemented!()
    }

    fn delete_objects(&self, input: &DeleteObjectsRequest) -> Result<DeleteObjectsOutput, DeleteObjectsError> {
        unimplemented!()
    }

    fn get_bucket_accelerate_configuration(&self, input: &GetBucketAccelerateConfigurationRequest) -> Result<GetBucketAccelerateConfigurationOutput, GetBucketAccelerateConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_acl(&self, input: &GetBucketAclRequest) -> Result<GetBucketAclOutput, GetBucketAclError> {
        unimplemented!()
    }

    fn get_bucket_analytics_configuration(&self, input: &GetBucketAnalyticsConfigurationRequest) -> Result<GetBucketAnalyticsConfigurationOutput, GetBucketAnalyticsConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_cors(&self, input: &GetBucketCorsRequest) -> Result<GetBucketCorsOutput, GetBucketCorsError> {
        unimplemented!()
    }

    fn get_bucket_inventory_configuration(&self, input: &GetBucketInventoryConfigurationRequest) -> Result<GetBucketInventoryConfigurationOutput, GetBucketInventoryConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_lifecycle(&self, input: &GetBucketLifecycleRequest) -> Result<GetBucketLifecycleOutput, GetBucketLifecycleError> {
        unimplemented!()
    }

    fn get_bucket_lifecycle_configuration(&self, input: &GetBucketLifecycleConfigurationRequest) -> Result<GetBucketLifecycleConfigurationOutput, GetBucketLifecycleConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_location(&self, input: &GetBucketLocationRequest) -> Result<GetBucketLocationOutput, GetBucketLocationError> {
        unimplemented!()
    }

    fn get_bucket_logging(&self, input: &GetBucketLoggingRequest) -> Result<GetBucketLoggingOutput, GetBucketLoggingError> {
        unimplemented!()
    }

    fn get_bucket_metrics_configuration(&self, input: &GetBucketMetricsConfigurationRequest) -> Result<GetBucketMetricsConfigurationOutput, GetBucketMetricsConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_notification(&self, input: &GetBucketNotificationConfigurationRequest) -> Result<NotificationConfigurationDeprecated, GetBucketNotificationError> {
        unimplemented!()
    }

    fn get_bucket_notification_configuration(&self, input: &GetBucketNotificationConfigurationRequest) -> Result<NotificationConfiguration, GetBucketNotificationConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_policy(&self, input: &GetBucketPolicyRequest) -> Result<GetBucketPolicyOutput, GetBucketPolicyError> {
        unimplemented!()
    }

    fn get_bucket_replication(&self, input: &GetBucketReplicationRequest) -> Result<GetBucketReplicationOutput, GetBucketReplicationError> {
        unimplemented!()
    }

    fn get_bucket_request_payment(&self, input: &GetBucketRequestPaymentRequest) -> Result<GetBucketRequestPaymentOutput, GetBucketRequestPaymentError> {
        unimplemented!()
    }

    fn get_bucket_tagging(&self, input: &GetBucketTaggingRequest) -> Result<GetBucketTaggingOutput, GetBucketTaggingError> {
        unimplemented!()
    }

    fn get_bucket_versioning(&self, input: &GetBucketVersioningRequest) -> Result<GetBucketVersioningOutput, GetBucketVersioningError> {
        unimplemented!()
    }

    fn get_bucket_website(&self, input: &GetBucketWebsiteRequest) -> Result<GetBucketWebsiteOutput, GetBucketWebsiteError> {
        unimplemented!()
    }



    fn get_object(&self, input: &GetObjectRequest) -> Result<GetObjectOutput, GetObjectError> {
        let f = File::open(&self.file_name).expect("file not found");

        Ok(GetObjectOutput{
            accept_ranges: None,
            body: Some(StreamingBody::new(f)),
            cache_control: None,
            content_disposition: None,
            content_encoding: None,
            content_language: None,
            content_length: None,
            content_range: None,
            content_type: None,
            delete_marker: None,
            e_tag: None,
            expiration: None,
            expires: None,
            last_modified: None,
            metadata: None,
            missing_meta: None,
            parts_count: None,
            replication_status: None,
            request_charged: None,
            restore: None,
            sse_customer_algorithm: None,
            sse_customer_key_md5: None,
            ssekms_key_id: None,
            server_side_encryption: None,
            storage_class: None,
            tag_count: None,
            version_id: None,
            website_redirect_location: None,
        })
    }

    fn get_object_acl(&self, input: &GetObjectAclRequest) -> Result<GetObjectAclOutput, GetObjectAclError> {
        unimplemented!()
    }

    fn get_object_tagging(&self, input: &GetObjectTaggingRequest) -> Result<GetObjectTaggingOutput, GetObjectTaggingError> {
        unimplemented!()
    }

    fn get_object_torrent(&self, input: &GetObjectTorrentRequest) -> Result<GetObjectTorrentOutput, GetObjectTorrentError> {
        unimplemented!()
    }

    fn head_bucket(&self, input: &HeadBucketRequest) -> Result<(), HeadBucketError> {
        unimplemented!()
    }

    fn head_object(&self, input: &HeadObjectRequest) -> Result<HeadObjectOutput, HeadObjectError> {
        unimplemented!()
    }

    fn list_bucket_analytics_configurations(&self, input: &ListBucketAnalyticsConfigurationsRequest) -> Result<ListBucketAnalyticsConfigurationsOutput, ListBucketAnalyticsConfigurationsError> {
        unimplemented!()
    }

    fn list_bucket_inventory_configurations(&self, input: &ListBucketInventoryConfigurationsRequest) -> Result<ListBucketInventoryConfigurationsOutput, ListBucketInventoryConfigurationsError> {
        unimplemented!()
    }

    fn list_bucket_metrics_configurations(&self, input: &ListBucketMetricsConfigurationsRequest) -> Result<ListBucketMetricsConfigurationsOutput, ListBucketMetricsConfigurationsError> {
        unimplemented!()
    }

    fn list_buckets(&self) -> Result<ListBucketsOutput, ListBucketsError> {
        unimplemented!()
    }

    fn list_multipart_uploads(&self, input: &ListMultipartUploadsRequest) -> Result<ListMultipartUploadsOutput, ListMultipartUploadsError> {
        unimplemented!()
    }

    fn list_object_versions(&self, input: &ListObjectVersionsRequest) -> Result<ListObjectVersionsOutput, ListObjectVersionsError> {
        unimplemented!()
    }

    fn list_objects(&self, input: &ListObjectsRequest) -> Result<ListObjectsOutput, ListObjectsError> {
        unimplemented!()
    }

    fn list_objects_v2(&self, input: &ListObjectsV2Request) -> Result<ListObjectsV2Output, ListObjectsV2Error> {
        unimplemented!()
    }

    fn list_parts(&self, input: &ListPartsRequest) -> Result<ListPartsOutput, ListPartsError> {
        unimplemented!()
    }

    fn put_bucket_accelerate_configuration(&self, input: &PutBucketAccelerateConfigurationRequest) -> Result<(), PutBucketAccelerateConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_acl(&self, input: &PutBucketAclRequest) -> Result<(), PutBucketAclError> {
        unimplemented!()
    }

    fn put_bucket_analytics_configuration(&self, input: &PutBucketAnalyticsConfigurationRequest) -> Result<(), PutBucketAnalyticsConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_cors(&self, input: &PutBucketCorsRequest) -> Result<(), PutBucketCorsError> {
        unimplemented!()
    }

    fn put_bucket_inventory_configuration(&self, input: &PutBucketInventoryConfigurationRequest) -> Result<(), PutBucketInventoryConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_lifecycle(&self, input: &PutBucketLifecycleRequest) -> Result<(), PutBucketLifecycleError> {
        unimplemented!()
    }

    fn put_bucket_lifecycle_configuration(&self, input: &PutBucketLifecycleConfigurationRequest) -> Result<(), PutBucketLifecycleConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_logging(&self, input: &PutBucketLoggingRequest) -> Result<(), PutBucketLoggingError> {
        unimplemented!()
    }

    fn put_bucket_metrics_configuration(&self, input: &PutBucketMetricsConfigurationRequest) -> Result<(), PutBucketMetricsConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_notification(&self, input: &PutBucketNotificationRequest) -> Result<(), PutBucketNotificationError> {
        unimplemented!()
    }

    fn put_bucket_notification_configuration(&self, input: &PutBucketNotificationConfigurationRequest) -> Result<(), PutBucketNotificationConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_policy(&self, input: &PutBucketPolicyRequest) -> Result<(), PutBucketPolicyError> {
        unimplemented!()
    }

    fn put_bucket_replication(&self, input: &PutBucketReplicationRequest) -> Result<(), PutBucketReplicationError> {
        unimplemented!()
    }

    fn put_bucket_request_payment(&self, input: &PutBucketRequestPaymentRequest) -> Result<(), PutBucketRequestPaymentError> {
        unimplemented!()
    }

    fn put_bucket_tagging(&self, input: &PutBucketTaggingRequest) -> Result<(), PutBucketTaggingError> {
        unimplemented!()
    }

    fn put_bucket_versioning(&self, input: &PutBucketVersioningRequest) -> Result<(), PutBucketVersioningError> {
        unimplemented!()
    }

    fn put_bucket_website(&self, input: &PutBucketWebsiteRequest) -> Result<(), PutBucketWebsiteError> {
        unimplemented!()
    }

    fn put_object(&self, input: &PutObjectRequest) -> Result<PutObjectOutput, PutObjectError> {
        unimplemented!()
    }

    fn put_object_acl(&self, input: &PutObjectAclRequest) -> Result<PutObjectAclOutput, PutObjectAclError> {
        unimplemented!()
    }

    fn put_object_tagging(&self, input: &PutObjectTaggingRequest) -> Result<PutObjectTaggingOutput, PutObjectTaggingError> {
        unimplemented!()
    }

    fn restore_object(&self, input: &RestoreObjectRequest) -> Result<RestoreObjectOutput, RestoreObjectError> {
        unimplemented!()
    }

    fn upload_part(&self, input: &UploadPartRequest) -> Result<UploadPartOutput, UploadPartError> {
        unimplemented!()
    }

    fn upload_part_copy(&self, input: &UploadPartCopyRequest) -> Result<UploadPartCopyOutput, UploadPartCopyError> {
        unimplemented!()
    }
}


pub struct GeneratedFileS3Client {
    pub lines: u32
}


#[allow(unused_variables, warnings)]
impl S3 for GeneratedFileS3Client{
    fn abort_multipart_upload(&self, input: &AbortMultipartUploadRequest) -> Result<AbortMultipartUploadOutput, AbortMultipartUploadError> {
        unimplemented!()
    }

    fn complete_multipart_upload(&self, input: &CompleteMultipartUploadRequest) -> Result<CompleteMultipartUploadOutput, CompleteMultipartUploadError> {
        unimplemented!()
    }

    fn copy_object(&self, input: &CopyObjectRequest) -> Result<CopyObjectOutput, CopyObjectError> {
        unimplemented!()
    }

    fn create_bucket(&self, input: &CreateBucketRequest) -> Result<CreateBucketOutput, CreateBucketError> {
        unimplemented!()
    }

    fn create_multipart_upload(&self, input: &CreateMultipartUploadRequest) -> Result<CreateMultipartUploadOutput, CreateMultipartUploadError> {
        unimplemented!()
    }

    fn delete_bucket(&self, input: &DeleteBucketRequest) -> Result<(), DeleteBucketError> {
        unimplemented!()
    }

    fn delete_bucket_analytics_configuration(&self, input: &DeleteBucketAnalyticsConfigurationRequest) -> Result<(), DeleteBucketAnalyticsConfigurationError> {
        unimplemented!()
    }

    fn delete_bucket_cors(&self, input: &DeleteBucketCorsRequest) -> Result<(), DeleteBucketCorsError> {
        unimplemented!()
    }

    fn delete_bucket_inventory_configuration(&self, input: &DeleteBucketInventoryConfigurationRequest) -> Result<(), DeleteBucketInventoryConfigurationError> {
        unimplemented!()
    }

    fn delete_bucket_lifecycle(&self, input: &DeleteBucketLifecycleRequest) -> Result<(), DeleteBucketLifecycleError> {
        unimplemented!()
    }

    fn delete_bucket_metrics_configuration(&self, input: &DeleteBucketMetricsConfigurationRequest) -> Result<(), DeleteBucketMetricsConfigurationError> {
        unimplemented!()
    }

    fn delete_bucket_policy(&self, input: &DeleteBucketPolicyRequest) -> Result<(), DeleteBucketPolicyError> {
        unimplemented!()
    }

    fn delete_bucket_replication(&self, input: &DeleteBucketReplicationRequest) -> Result<(), DeleteBucketReplicationError> {
        unimplemented!()
    }

    fn delete_bucket_tagging(&self, input: &DeleteBucketTaggingRequest) -> Result<(), DeleteBucketTaggingError> {
        unimplemented!()
    }

    fn delete_bucket_website(&self, input: &DeleteBucketWebsiteRequest) -> Result<(), DeleteBucketWebsiteError> {
        unimplemented!()
    }

    fn delete_object(&self, input: &DeleteObjectRequest) -> Result<DeleteObjectOutput, DeleteObjectError> {
        unimplemented!()
    }

    fn delete_object_tagging(&self, input: &DeleteObjectTaggingRequest) -> Result<DeleteObjectTaggingOutput, DeleteObjectTaggingError> {
        unimplemented!()
    }

    fn delete_objects(&self, input: &DeleteObjectsRequest) -> Result<DeleteObjectsOutput, DeleteObjectsError> {
        unimplemented!()
    }

    fn get_bucket_accelerate_configuration(&self, input: &GetBucketAccelerateConfigurationRequest) -> Result<GetBucketAccelerateConfigurationOutput, GetBucketAccelerateConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_acl(&self, input: &GetBucketAclRequest) -> Result<GetBucketAclOutput, GetBucketAclError> {
        unimplemented!()
    }

    fn get_bucket_analytics_configuration(&self, input: &GetBucketAnalyticsConfigurationRequest) -> Result<GetBucketAnalyticsConfigurationOutput, GetBucketAnalyticsConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_cors(&self, input: &GetBucketCorsRequest) -> Result<GetBucketCorsOutput, GetBucketCorsError> {
        unimplemented!()
    }

    fn get_bucket_inventory_configuration(&self, input: &GetBucketInventoryConfigurationRequest) -> Result<GetBucketInventoryConfigurationOutput, GetBucketInventoryConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_lifecycle(&self, input: &GetBucketLifecycleRequest) -> Result<GetBucketLifecycleOutput, GetBucketLifecycleError> {
        unimplemented!()
    }

    fn get_bucket_lifecycle_configuration(&self, input: &GetBucketLifecycleConfigurationRequest) -> Result<GetBucketLifecycleConfigurationOutput, GetBucketLifecycleConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_location(&self, input: &GetBucketLocationRequest) -> Result<GetBucketLocationOutput, GetBucketLocationError> {
        unimplemented!()
    }

    fn get_bucket_logging(&self, input: &GetBucketLoggingRequest) -> Result<GetBucketLoggingOutput, GetBucketLoggingError> {
        unimplemented!()
    }

    fn get_bucket_metrics_configuration(&self, input: &GetBucketMetricsConfigurationRequest) -> Result<GetBucketMetricsConfigurationOutput, GetBucketMetricsConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_notification(&self, input: &GetBucketNotificationConfigurationRequest) -> Result<NotificationConfigurationDeprecated, GetBucketNotificationError> {
        unimplemented!()
    }

    fn get_bucket_notification_configuration(&self, input: &GetBucketNotificationConfigurationRequest) -> Result<NotificationConfiguration, GetBucketNotificationConfigurationError> {
        unimplemented!()
    }

    fn get_bucket_policy(&self, input: &GetBucketPolicyRequest) -> Result<GetBucketPolicyOutput, GetBucketPolicyError> {
        unimplemented!()
    }

    fn get_bucket_replication(&self, input: &GetBucketReplicationRequest) -> Result<GetBucketReplicationOutput, GetBucketReplicationError> {
        unimplemented!()
    }

    fn get_bucket_request_payment(&self, input: &GetBucketRequestPaymentRequest) -> Result<GetBucketRequestPaymentOutput, GetBucketRequestPaymentError> {
        unimplemented!()
    }

    fn get_bucket_tagging(&self, input: &GetBucketTaggingRequest) -> Result<GetBucketTaggingOutput, GetBucketTaggingError> {
        unimplemented!()
    }

    fn get_bucket_versioning(&self, input: &GetBucketVersioningRequest) -> Result<GetBucketVersioningOutput, GetBucketVersioningError> {
        unimplemented!()
    }

    fn get_bucket_website(&self, input: &GetBucketWebsiteRequest) -> Result<GetBucketWebsiteOutput, GetBucketWebsiteError> {
        unimplemented!()
    }



    fn get_object(&self, input: &GetObjectRequest) -> Result<GetObjectOutput, GetObjectError> {
//        let f = File::open(&self.file_name).expect("file not found");
        let mut reader = OnDemandReader{
            lines: self.lines,
            sent: 0
        };

        Ok(GetObjectOutput{
            accept_ranges: None,
            body: Some(StreamingBody::new(reader)),
            cache_control: None,
            content_disposition: None,
            content_encoding: None,
            content_language: None,
            content_length: None,
            content_range: None,
            content_type: None,
            delete_marker: None,
            e_tag: None,
            expiration: None,
            expires: None,
            last_modified: None,
            metadata: None,
            missing_meta: None,
            parts_count: None,
            replication_status: None,
            request_charged: None,
            restore: None,
            sse_customer_algorithm: None,
            sse_customer_key_md5: None,
            ssekms_key_id: None,
            server_side_encryption: None,
            storage_class: None,
            tag_count: None,
            version_id: None,
            website_redirect_location: None,
        })
    }

    fn get_object_acl(&self, input: &GetObjectAclRequest) -> Result<GetObjectAclOutput, GetObjectAclError> {
        unimplemented!()
    }

    fn get_object_tagging(&self, input: &GetObjectTaggingRequest) -> Result<GetObjectTaggingOutput, GetObjectTaggingError> {
        unimplemented!()
    }

    fn get_object_torrent(&self, input: &GetObjectTorrentRequest) -> Result<GetObjectTorrentOutput, GetObjectTorrentError> {
        unimplemented!()
    }

    fn head_bucket(&self, input: &HeadBucketRequest) -> Result<(), HeadBucketError> {
        unimplemented!()
    }

    fn head_object(&self, input: &HeadObjectRequest) -> Result<HeadObjectOutput, HeadObjectError> {
        unimplemented!()
    }

    fn list_bucket_analytics_configurations(&self, input: &ListBucketAnalyticsConfigurationsRequest) -> Result<ListBucketAnalyticsConfigurationsOutput, ListBucketAnalyticsConfigurationsError> {
        unimplemented!()
    }

    fn list_bucket_inventory_configurations(&self, input: &ListBucketInventoryConfigurationsRequest) -> Result<ListBucketInventoryConfigurationsOutput, ListBucketInventoryConfigurationsError> {
        unimplemented!()
    }

    fn list_bucket_metrics_configurations(&self, input: &ListBucketMetricsConfigurationsRequest) -> Result<ListBucketMetricsConfigurationsOutput, ListBucketMetricsConfigurationsError> {
        unimplemented!()
    }

    fn list_buckets(&self) -> Result<ListBucketsOutput, ListBucketsError> {
        unimplemented!()
    }

    fn list_multipart_uploads(&self, input: &ListMultipartUploadsRequest) -> Result<ListMultipartUploadsOutput, ListMultipartUploadsError> {
        unimplemented!()
    }

    fn list_object_versions(&self, input: &ListObjectVersionsRequest) -> Result<ListObjectVersionsOutput, ListObjectVersionsError> {
        unimplemented!()
    }

    fn list_objects(&self, input: &ListObjectsRequest) -> Result<ListObjectsOutput, ListObjectsError> {
        unimplemented!()
    }

    fn list_objects_v2(&self, input: &ListObjectsV2Request) -> Result<ListObjectsV2Output, ListObjectsV2Error> {
        unimplemented!()
    }

    fn list_parts(&self, input: &ListPartsRequest) -> Result<ListPartsOutput, ListPartsError> {
        unimplemented!()
    }

    fn put_bucket_accelerate_configuration(&self, input: &PutBucketAccelerateConfigurationRequest) -> Result<(), PutBucketAccelerateConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_acl(&self, input: &PutBucketAclRequest) -> Result<(), PutBucketAclError> {
        unimplemented!()
    }

    fn put_bucket_analytics_configuration(&self, input: &PutBucketAnalyticsConfigurationRequest) -> Result<(), PutBucketAnalyticsConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_cors(&self, input: &PutBucketCorsRequest) -> Result<(), PutBucketCorsError> {
        unimplemented!()
    }

    fn put_bucket_inventory_configuration(&self, input: &PutBucketInventoryConfigurationRequest) -> Result<(), PutBucketInventoryConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_lifecycle(&self, input: &PutBucketLifecycleRequest) -> Result<(), PutBucketLifecycleError> {
        unimplemented!()
    }

    fn put_bucket_lifecycle_configuration(&self, input: &PutBucketLifecycleConfigurationRequest) -> Result<(), PutBucketLifecycleConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_logging(&self, input: &PutBucketLoggingRequest) -> Result<(), PutBucketLoggingError> {
        unimplemented!()
    }

    fn put_bucket_metrics_configuration(&self, input: &PutBucketMetricsConfigurationRequest) -> Result<(), PutBucketMetricsConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_notification(&self, input: &PutBucketNotificationRequest) -> Result<(), PutBucketNotificationError> {
        unimplemented!()
    }

    fn put_bucket_notification_configuration(&self, input: &PutBucketNotificationConfigurationRequest) -> Result<(), PutBucketNotificationConfigurationError> {
        unimplemented!()
    }

    fn put_bucket_policy(&self, input: &PutBucketPolicyRequest) -> Result<(), PutBucketPolicyError> {
        unimplemented!()
    }

    fn put_bucket_replication(&self, input: &PutBucketReplicationRequest) -> Result<(), PutBucketReplicationError> {
        unimplemented!()
    }

    fn put_bucket_request_payment(&self, input: &PutBucketRequestPaymentRequest) -> Result<(), PutBucketRequestPaymentError> {
        unimplemented!()
    }

    fn put_bucket_tagging(&self, input: &PutBucketTaggingRequest) -> Result<(), PutBucketTaggingError> {
        unimplemented!()
    }

    fn put_bucket_versioning(&self, input: &PutBucketVersioningRequest) -> Result<(), PutBucketVersioningError> {
        unimplemented!()
    }

    fn put_bucket_website(&self, input: &PutBucketWebsiteRequest) -> Result<(), PutBucketWebsiteError> {
        unimplemented!()
    }

    fn put_object(&self, input: &PutObjectRequest) -> Result<PutObjectOutput, PutObjectError> {
        unimplemented!()
    }

    fn put_object_acl(&self, input: &PutObjectAclRequest) -> Result<PutObjectAclOutput, PutObjectAclError> {
        unimplemented!()
    }

    fn put_object_tagging(&self, input: &PutObjectTaggingRequest) -> Result<PutObjectTaggingOutput, PutObjectTaggingError> {
        unimplemented!()
    }

    fn restore_object(&self, input: &RestoreObjectRequest) -> Result<RestoreObjectOutput, RestoreObjectError> {
        unimplemented!()
    }

    fn upload_part(&self, input: &UploadPartRequest) -> Result<UploadPartOutput, UploadPartError> {
        unimplemented!()
    }

    fn upload_part_copy(&self, input: &UploadPartCopyRequest) -> Result<UploadPartCopyOutput, UploadPartCopyError> {
        unimplemented!()
    }
}

struct OnDemandReader {
    lines: u32,
    sent: u32
}

impl Read for OnDemandReader{
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {

        if self.sent >= self.lines {
            return Ok(0);
        }
        println!("Length: {}", buf.len());

        let line =  if self.sent == 0 {
            "#Version: 1.0\n".as_bytes()
        } else {
            "2018-01-07      21:25:16        IAD12   341     34.200.244.134  GET     d3noeubizwmewx.cloudfront.net   /       304     -       Mozilla/5.0%2520(X11;%2520Linux%2520x86_64;%2520rv:52.0)%2520Gecko/20100101%2520Firefox/52.0    -       -       Hit     N7TQJQHeepMeKKa8lgfE_h4cfzbjtDxAjWtRZ7ESyWdrp_xWub0DiQ==        oqqer.com       https   234     0.003   -       TLSv1.2 ECDHE-RSA-AES128-GCM-SHA256     Hit     HTTP/2.0\n".as_bytes()
        };

        for x in 0..line.len() {
            buf[x] = line[x];
        }

        self.sent +=1;

        Ok(line.len())
    }
}