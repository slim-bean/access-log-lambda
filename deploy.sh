#!/bin/bash -e

IP=$(cat ./build_ip.txt)

printf "\n#### Syncing files ####\n"

rsync -rt --exclude=.git --exclude=dist --exclude=target --exclude=.idea --exclude=ansible --exclude=venv  ./ ec2-user@${IP}:access-log/

printf "\n#### Setting up output and copying python files ####\n"

ssh ec2-user@${IP} "cd access-log; rm -rf dist/; mkdir -p dist/access-log; cp python/handler.py dist/access-log/"

printf "\n#### Running cargo build ####\n"

ssh ec2-user@${IP} "cd access-log;RUST_BACKTRACE=yes OPENSSL_STATIC=1 OPENSSL_DIR=/home/ec2-user/openssl/out/ cargo build --release; cp target/release/libaccessloglambda.so dist/access-log/"

printf "\n#### Creating zip ####\n"

ssh ec2-user@${IP} "cd access-log; zip -rj dist/access-log.zip dist/access-log/"

printf "\n#### Deploying Lambda ####\n"

ssh ec2-user@${IP} "cd access-log; aws lambda update-function-code --function-name access-log --zip-file fileb://dist/access-log.zip"

printf "\n#### Finished ####\n"

