from ctypes import cdll
from os import environ

try:
    lib = cdll.LoadLibrary(environ['SO_PATH'])
except KeyError:
    lib = cdll.LoadLibrary('./libaccessloglambda.so')


def handler(event, context):
    print(event)
    print(context)
    for record in event['Records']:
        lib.handle(record['s3']['bucket']['name'].encode('utf-8'),
                   record['s3']['object']['key'].encode('utf-8'),
                   record['awsRegion'].encode('utf-8'))
